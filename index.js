const {Builder, By, Key } = require('selenium-webdriver');

async function main() {
  const driver = await new Builder().forBrowser('chrome').build();
  const actions = await driver.actions()
  try {
    await driver.get('https://airflstaging.ru/');
    const title = await driver.getTitle()
    if (title !== 'Биржа текста с гарантией результата. Рерайт, копирайт и редактура') throw new Error('Title check failed')

    const searchForm = await driver.findElement(By.id('main_input'));
    await searchForm.sendKeys('selenium')

    await actions.keyDown(Key.ENTER).perform()

    await driver.navigate().to('https://www.google.com/')
    await driver.navigate().back()

    // const link = await driver.findElement(By.className('dashed-link'))
    // await link.click()

    // await driver.executeScript('window.open("");');
  } catch(e) {
    console.log(e)
  } finally {
    driver.close()
  }
};

main()